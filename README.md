# Projet Vue.js DAWIN


## Lancement du projet :

`npm install && node_modules/.bin/webpack --progress --hide-modules && node src/node/app.js`

## Accés au site :

http://localhost:3000/

## Structure de fichiers

```
package.json
webpack.config.js
node_modules/
src/
    |_ static/ # les images et fichiers CSS
    |_ node/
        |_ app.js # point d'entrée de l'application Node
        |_ routes.js # Routes de l'application
    |_ vue/
        |_ main.js # point d'entrée de l'application Vue.js
        |_ app.vue
        |_ routes.js # config de vue-router
        |_ components/
            |_ movie-item.vue
            |_ ...
    |_ dist/ # Sortie de la compilation avec Webpack
uploads/ # Posters
```
## Structure des pages

```
/ -> Liste des films avec recherche (Détails / édition / suppression)
/movie/:id -> Détails d'un film (avec lien vers édition, suppression, notation)
/movie/:id/edit -> Modification d'un film (infos + rating) + ajout d'un poster
```

## Fonctionnalités

 - Ajout, suppression et édition des informations d’un film
 - Affichage de la liste des films
 - Recherche à l’intérieur des différents paramètres (un seul champ de recherche)
 - Upload d’un poster pour chaque film
 - Possibilité de noter un film sur une échelle de 1 à 5

***
Sylvain Dupuy (contact@dupuysylvain.fr)
***