const express = require('express')
const bodyParser = require('body-parser');
var multer = require('multer')
var path = require('path')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
})

var upload = multer({
    storage: storage
});

const app = express();

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

var movies = [{
        id: 0,
        title: "L'étrange noël de M. Jack",
        year: 1994,
        language: "Anglais",
        rating: 3,
        synopsys: "Jack Skellington, un épouvantail squelettique surnommé « le Roi des citrouilles » (Pumpkin King en version originale), vit dans la ville d'Halloween. En tant que maître de l'épouvante, Jack occupe ses journées à préparer la prochaine fête d'Halloween.",
        director: {
            name: "Henry",
            firstName: "Selick",
            nationality: "Américain",
            birthday: "30/11/1952"
        },
        poster: "/uploads/jack.jpg",
        genre: "Animation"
    },
    {
        id: 2,
        title: "Interstellar",
        year: 2014,
        language: "Francais",
        rating: 4,
        director: {
            name: "Christopher",
            firstName: "Nolan",
            nationality: "Britanique",
            birthday: "30/07/1970"
        },
        synopsys: "Alors que la Terre se meurt, une équipe d'astronautes franchit un trou de ver apparu près de Saturne conduisant à une autre galaxie, cela dans le but d'explorer un nouveau système stellaire et l'espoir de trouver une nouvelle planète habitable par l'humanité afin de la sauver.",
        poster: "/uploads/interstellar.jpg",
        genre: "Science fiction"
    },
    {
        id: 1,
        title: "FORREST GUMP",
        year: 2012,
        language: "Anglais",
        rating: 3,
        director: {
            name: "ROBERT",
            firstName: "ZEMECKIS",
            nationality: "Américain",
            birthday: "14/05/1952"
        },
        synopsys: "Quelques décennies d'histoire américaine, des années 1940 à la fin du XXème siècle, à travers le regard et l'étrange odyssée d'un homme simple et pur, Forrest Gump.",
        poster: "/uploads/forrest_gump.jpg",
        genre: "Comédie dramatique"
    },
    {
        title: "La Ligne verte",
        year: 1999,
        id: 3,
        language: "Francais",
        rating: 5,
        director: {
            name: "FRANK",
            firstName: "DARABONT",
            nationality: "Américain",
            birthday: "28/01/1959"
        },
        synopsys: "Paul Edgecomb, Gardien-chef du pénitencier de Cold Mountain en 1935, était chargé de veiller au bon déroulement des exécutions capitales. Parmi les prisonniers se trouvait un colosse du nom de John Coffey...",
        poster: "/uploads/ligne_verte.jpg",
        genre: "Fantastique"
    },
    {
        title: "YOUR NAME",
        year: 2016,
        id: 4,
        language: "Japonais",
        rating: 4,
        director: {
            name: "MAKOTO",
            firstName: "SHINKAI",
            nationality: "Japonais",
            birthday: "09/02/1973"
        },
        synopsys: "Mitsuha, adolescente coincée dans une famille traditionnelle, rêve de quitter ses montagnes natales pour découvrir la vie trépidante de Tokyo. Elle est loin d’imaginer pouvoir vivre l’aventure urbaine dans la peau de… Taki, un jeune lycéen vivant à Tokyo, occupé entre son petit boulot dans un restaurant italien et ses nombreux amis. À travers ses rêves, Mitsuha se voit littéralement propulsée dans la vie du jeune garçon au point qu’elle croit vivre la réalité... Tout bascule lorsqu’elle réalise que Taki rêve également d’une vie dans les montagnes, entouré d’une famille traditionnelle… dans la peau d’une jeune fille ! Une étrange relation s’installe entre leurs deux corps qu’ils accaparent mutuellement. Quel mystère se cache derrière ces rêves étranges qui unissent deux destinées que tout oppose et qui ne se sont jamais rencontrées ?",
        poster: "/uploads/your_name.png",
        genre: "Animation"
    },
    {
        title: "COCO",
        year: 2017,
        id: 5,
        language: "Anglais",
        rating: 3,
        director: {
            name: "LEE",
            firstName: "UNKRICH",
            nationality: "Américain",
            birthday: "08/08/1967"
        },
        synopsys: "Diplômé de l'école de cinéma de l' USC (University of Southern California) en 1991, Lee Unkrich entame sa carrière dans le cinéma et la télévision en prises de vue réelles, travaillant notamment sur la série Les Dessous de Palm Beach. Arrivé en 1994 chez Pixar, il travaille comme monteur sur Toy Story en 1995, puis sur 1001 Pattes en 1998, pour lequel il assure également certaines voix additionnelles. Co-réalisateur, monteur et à nouveau voix additionnelles sur Toy Story 2 en 1999, Lee Unkrich co-signe Monstres & Cie en 2001. Deux ans plus tard, il coréalise avec Andrew Stanton, Le Monde de Nemo, certifié plus grand succès de l'histoire de l'animation quelques semaines après sa sortie. L'année 2010 représente un tournant important dans sa carrière, puisqu'il est nommé à la tête de la réalisation de Toy Story 3, le troisième volet des aventures des célèbres jouets animés.",
        poster: "/uploads/coco.jpg",
        genre: "Fantastique"
    }
];

app.use(bodyParser.json()) //parse JSON body
app.use(bodyParser.urlencoded()) //parse x-www-form-urlencoded body

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    var now = new Date();
    console.log(now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + ", " + req.url);
    console.dir(req.body);
    req.date = now;
    next();
})

app.get('/', (req, res) => {
    res.sendFile("index.html", {
        root: "./src/static"
    });
})

app.get('/build.js', (req, res) => {
    res.sendFile("./build.js", {
        root: "./src/dist"
    });
})

// Poster
app.get('/uploads/:fileName', (req, res) => {
    console.log(req.params.fileName);

    res.sendFile(req.params.fileName, {
        root: "./uploads"
    });
})

app.get('/api/movies/all', (req, res) => {
    res.json(movies);
})

app.get('/api/movies/:id', (req, res) => {
    res.json(movies.find(el => el.id == req.params.id));
});

app.put('/api/movies/:id', (req, res) => {

    var movie = movies.find(el => el.id == req.params.id);

    if (movie) {
        movie.title = req.body.title;
        movie.year = req.body.year;
        movie.synopsys = req.body.synopsys;
        movie.rating = req.body.rating;
        movie.language = req.body.language;
        movie.director = req.body.director;
        movie.genre = req.body.genre;

        res.sendStatus(204);
    } else {
        res.sendStatus(404);
    }
});

app.delete('/api/movies/:id', (req, res) => {
    var movie = movies.find(el => el.id == req.params.id);
    var index = movies.indexOf(movie);

    if (movie) {
        movies.splice(index, 1);
        res.sendStatus(204);
    }
});

app.post('/api/movies', (req, res) => {
    movies.push({
        id: getRandomInt(Number.MAX_SAFE_INTEGER),
        title: req.body.title,
        year: req.body.year,
        synopsys: req.body.synopsys,
        rating: req.body.rating,
        language: req.body.language,
        director: req.body.director,
        genre: req.body.genre
    });
    res.sendStatus(201);
})

// Poster
app.post('/api/movies/:id/poster', upload.single('poster'), function (req, res) {
    var movie = movies.find(el => el.id == req.params.id);

    if (movie) {
        movie.poster = "/uploads/" + req.file.filename;
        res.sendStatus(201);
    }
})

app.listen(3000)