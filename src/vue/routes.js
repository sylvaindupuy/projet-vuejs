import Vue from 'vue'
import Router from 'vue-router'
import HomeComponent from "./components/home.vue";
import EditComponent from "./components/edit.vue";
import movieComponent from "./components/movie.vue";

Vue.use(Router)

export default new Router({
    routes: [{
            path: "/",
            name: 'home',
            component: HomeComponent
        },
        {
            path: "/movie/:id",
            name: "movie",
            component: movieComponent
        },
        {
            path: "/movie/:id/edit",
            name: "edit",
            component: EditComponent
        }
    ]
})