import Vue from 'vue' //librairie "vue" dans node_modules
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import app from './app.vue' //fichier app.vue local
import MovieItemComponent from './components/movieitem.vue'
import router from './routes.js'
import Axios from 'axios'

Vue.use(Vuetify)
Vue.component('movie-item', MovieItemComponent);


window.shared = {
    movies: undefined,
    getMovies: function (callback) {
        Axios.get("http://localhost:3000/api/movies/all").then(function (response) {
            if (response.status === 200) {
                if (callback instanceof Function) {
                    callback(response.data);
                }
            }
        });
    },
    getMovieById: function (id, callback) {
        Axios.get("http://localhost:3000/api/movies/" + id).then(function (response) {
            if (response.status === 200) {
                if (callback instanceof Function) {
                    callback(response.data);
                }
            }
        });
    },
    updateMovie: function (id, movie, callback) {
        Axios.put("http://localhost:3000/api/movies/" + id, movie).then(function (response) {
            if (response.status === 204) {
                if (callback instanceof Function) {
                    callback(response.data);
                }
            }
        });
    },
    addMovie: function (movie, callback) {
        Axios.post("http://localhost:3000/api/movies/", movie).then(function (response) {
            if (response.status === 201) {
                if (callback instanceof Function) {
                    callback(response.data);
                }
            }
        });
    },
    removeMovie: function (id, callback) {
        Axios.delete("http://localhost:3000/api/movies/" + id).then(function (response) {
            if (response.status === 204) {
                if (callback instanceof Function) {
                    callback(response.data);
                }
            }
        });
    },
    addPoster: function (id, image, callback) {
        Axios.post("/api/movies/" + id + "/poster", image).then(function (response) {
            if (response.status === 201) {
                if (callback instanceof Function) {
                    callback(response.data);
                }
            }
        });
    }
};

new Vue({
    el: '#app',
    router,
    render: h => h(app)
})